import React from 'react';
import ReactDOM from 'react-dom';
import Index from './day6/view/index';

ReactDOM.render(
    <Index />, 
    document.getElementById('root')
    );
