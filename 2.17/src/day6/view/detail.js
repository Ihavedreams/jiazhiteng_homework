import React, {Component} from 'react'
import Task from './task'

class Detail extends Component {
    changeContent = () => {
        console.log('恭喜您，点击事件成功了！')
        let {
            changeContent
        } = this.props;
        changeContent('子组件传递的东西');
    }
    render () {
        let {
            content,
            changeContent,
            sendDatailData
        } = this.props;
        return (
            <div>
               detail页面
               <br></br>
               {
                   sendDatailData
               }
               <button onClick={this.changeContent}>改变content</button>
               <Task changeContent={changeContent} />
            </div>
        )
    }
}

export default Detail