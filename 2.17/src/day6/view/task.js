import React, { Component } from 'react'

class Task extends Component {
    childChange = () => {
        console.log('恭喜你也修改成功了');
        let {
            changeContent
        } = this.props;
        changeContent('task修改用的')
    }
    
    render () {
        return (
            <div>
                <button onClick={this.childChange}>我也要修改</button>
            </div>
        )
    }
}

export default Task