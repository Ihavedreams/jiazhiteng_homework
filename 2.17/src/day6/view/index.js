import React, {Component} from 'react'
import Detail from './detail'
import Task from './task'

class Index extends Component {
    constructor (props) {
        super(props)
        this.state = {
            content: '给子组件传递一些东西',
            sendDetalDate: ''
        }
    }

    changeContent = (content) => {
        this.setState({
            content
        })
    }

    getTaskData = (data)=> {
        this.setState({
            sendDatailData: data
        })
    }

    render () {
        let {
            content,
            sendDatailData
        } = this.state;
        return (
            <div>
                Index页面 
                <Detail sendDatailData={this.sendDatailData} content={content} changeContent={this.changeContent} />
                <Task getTaskData={this.getTaskData} />
            </div>
        )
    }
}

export default Index